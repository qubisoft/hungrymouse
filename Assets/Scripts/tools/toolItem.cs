﻿using UnityEngine;

public class toolItem : MonoBehaviour
{
    private Vector3 mOffset;
    private Vector3 posStart;
    private Vector3 scaleStart;
    private Quaternion rotStart;
    public bool isClicked = false;
    private bool scaled = true;
    public bool stop = false;      // lock dragging an abject after putting it in right place
    public string scriptName;

    private void Start()
    {
        getItemFromToolBox.SaveTransform(gameObject, ref posStart, ref rotStart, ref scaleStart);
    }
    private void OnMouseDown()
    {
        isClicked = true;

        getItemFromToolBox.MouseDown(gameObject, ref scaled, ref mOffset, ref stop);
    }
    private void OnMouseDrag()
    {
        getItemFromToolBox.MouseDrag(gameObject, ref mOffset, ref stop);
    }

    private void OnMouseUp()
    {
        getItemFromToolBox.MouseUp(gameObject, ref posStart, ref rotStart, ref scaleStart, ref scaled, ref stop);
    }
}
