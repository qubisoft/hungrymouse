﻿using UnityEngine;

public class trampoline : MonoBehaviour
{
    private bool bounce;
    private bool start = false;
    private bool onlyOneBounce = true;
    private bool onlyOneAnimation = true;
    private GameObject Player;
    private GameObject anim;

    private void Start()
    {
        anim = gameObject.transform.GetChild(0).gameObject;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;

        if (!start && (tag == tags.mouseRuns || tag == tags.enemyRuns))
        {
            Player = collision.gameObject;
            start = true;
        }
    }

    private void Update()
    {
        // stop waiting for player if he was destroyed
        if (Player == null)
            start = false;

        if (start)
        {
            // if player went on the trampoline
            if (onlyOneAnimation && (System.Math.Abs(gameObject.transform.position.x - Player.transform.position.x) < 1f))
            {
                anim.GetComponent<Animator>().SetTrigger("Now");
                onlyOneAnimation = false;
            }

            // if player went on the center of trampoline
            if (onlyOneBounce && (System.Math.Abs(gameObject.transform.position.x - Player.transform.position.x) < 0.1f))
            {
                onlyOneBounce = false;
                bounce = true;
            }

            if (bounce)
            {
                Vector3 vel = Player.GetComponent<Rigidbody2D>().velocity;
                vel.y = 0;
                Player.GetComponent<Rigidbody2D>().velocity = vel;
                Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(1.1f, staticClass.height), ForceMode2D.Impulse);
                bounce = false;
            }
        }
    }
}
