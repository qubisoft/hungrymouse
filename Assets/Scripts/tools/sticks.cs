﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sticks : MonoBehaviour
{
    private void Update()
    {
        gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
        GameObject hole = GameObject.FindGameObjectWithTag(tags.hole);
        if (hole != null)
            hole.GetComponent<BoxCollider2D>().isTrigger = true;
    }
}
