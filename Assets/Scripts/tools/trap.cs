﻿using UnityEngine;

public class trap : MonoBehaviour
{
    private GameObject enemy;
    private enemyScript enemyScript;

    private bool enemyExists = false;
    private bool ready = false;
    private bool onlyOnce = true;

    private GameObject anim;

    private void Start()
    {
        // find enemy to catch
        if (GameObject.FindGameObjectWithTag(tags.enemySleeps) != null)
        {
            enemy = GameObject.FindGameObjectWithTag(tags.enemy);
            enemyScript = enemy.GetComponent<enemyScript>();
            if (gameObject.tag == tags.cage)
                enemyExists = enemyScript.enemySO.cageReact;
            if (gameObject.tag == tags.wool)
                enemyExists = enemyScript.enemySO.woolReact;
        }

        anim = gameObject.transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        if (gameObject.GetComponent<toolItem>().stop && onlyOnce)
        {
            // run animation
            anim.GetComponent<Animator>().SetBool("start",true);
            anim.GetComponent<Animator>().SetBool("stop", false);
            // check until anim completed
            if (anim.GetComponent<animEvent>() != null)
                ready = anim.GetComponent<animEvent>().ready;
            else
                ready = true;

            // catch enemy
            if (enemyExists && ready)
            {
                onlyOnce = false;
                enemyScript.enemyTarget = gameObject;
                enemyScript.wakeUp();
            }
        }
    }
 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // close cage if enemy trapped
        if (collision.gameObject.tag == tags.enemyTrapped)
        {
            anim.GetComponent<Animator>().SetBool("start", false);
            anim.GetComponent<Animator>().SetBool("stop", true);
        }
    }
}
