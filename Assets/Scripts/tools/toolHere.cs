﻿using System.Collections.Generic;
using UnityEngine;

public class toolHere : MonoBehaviour
{
    public Dictionary<string, bool> toolsUsed = new Dictionary<string, bool>();

    // need to be updated after addition of tools
    public bool tagStickHere;
    public bool tagFenceHere;
    public bool tagJumperHere;
    public bool tagWoolHere;
    public bool tagCageHere;
    public bool tagAxeHere;
    public bool tagSticksHere;
    public bool tagMatchHere;
    public bool tagBucketHere;

    // need to be updated after addition of tools
    private void Start()
    {
        toolsUsed[tags.stick]=tagStickHere;
        toolsUsed[tags.fence]= tagFenceHere;
        toolsUsed[tags.jumper]= tagJumperHere;
        toolsUsed[tags.wool]= tagWoolHere;
        toolsUsed[tags.cage]= tagCageHere;
        toolsUsed[tags.axe] = tagAxeHere;
        toolsUsed[tags.sticks] = tagSticksHere;
        toolsUsed[tags.match] = tagMatchHere;
        toolsUsed[tags.bucket] = tagBucketHere;
    }
}
