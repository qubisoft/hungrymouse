﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class axe : MonoBehaviour
{
    private bool onlyOnce = true;
    private bool axeStarts = true;
    private GameObject anim;
    private GameObject tree;
    public bool isDone = false;

    void Start()
    {
        tree = GameObject.FindGameObjectWithTag(tags.treeToCut);
        anim = gameObject.transform.GetChild(0).gameObject;
    }

    void Update()
    {
        if (gameObject.GetComponent<toolItem>().stop && onlyOnce)
        {
            anim.GetComponent<Animator>().SetTrigger("Active");
            anim.GetComponent<Animator>().SetBool("start",true);
            anim.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);
            anim.transform.parent.GetComponent<PolygonCollider2D>().isTrigger = true;
            onlyOnce = false;
            if (tree != null)
            {
                tree.GetComponent<Animator>().SetBool("start", true);
                tree.transform.Find("treeTop").GetComponent<PolygonCollider2D>().isTrigger = false;
            }
        }            
    }
}
