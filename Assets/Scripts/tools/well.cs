﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class well : MonoBehaviour
{
    GameObject[] flame;
    int flameCount;

    GameObject bucket;

    bool onlyOnce = true;
    bool bucketDone = false;

    void Start()
    {
        // run fire flames
        flame = GameObject.FindGameObjectsWithTag(tags.flame);
        flameCount = flame.Length;
        for (int i = 0; i < flameCount; i++)
            flame[i].GetComponent<Animator>().SetTrigger("fire");
        
    }

    void Update()
    {
        if (onlyOnce)
        {
            bucket = GameObject.FindGameObjectWithTag(tags.bucket);
            if (bucket != null && bucket.GetComponent<toolItem>().stop)
            {
                onlyOnce = false;
                gameObject.GetComponent<Animator>().SetTrigger("down");
            }
        }

        if (!bucketDone && bucket.transform.GetChild(0).GetComponent<bucketEvent>().done)
        {
            bucketDone = true;
            for (int i = 0; i < flameCount; i++)
            {
                flame[i].GetComponent<Animator>().SetTrigger("water");
                if (flame[i].GetComponent<PolygonCollider2D>() != null)
                    flame[i].GetComponent<PolygonCollider2D>().enabled = false;
            }
        }
    }
}
