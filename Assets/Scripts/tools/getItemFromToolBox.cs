﻿using UnityEngine;
using UnityEngine.Rendering;

public static class getItemFromToolBox
{
    private static float tolerance = 0.5f;
    public static void SaveTransform(this GameObject obj, ref Vector3 posStart, ref Quaternion rotStart, ref Vector3 scaleStart)
    {
        // read starting position, rotation and scale of item
        posStart = obj.transform.position;
        rotStart = obj.transform.rotation;
        scaleStart = obj.transform.localScale;
    }
    
    private static Vector3 GetMouseWorldPos()
    {
        // initial point of mouse
        Vector3 mousePoint = Input.mousePosition;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    public static void MouseDown(this GameObject obj, ref bool scaled, ref Vector3 mOffset, ref bool stop)
    {
        if (!stop)
        {
            // back to normal scale but do it once only
            if (scaled)
            {
                scaled = false;
                obj.transform.localScale = new Vector3(1, 1, 1);
                obj.transform.localRotation = Quaternion.identity;
            }

            // move the stick
            mOffset = obj.transform.position - GetMouseWorldPos();
        }
    }

    // drag the stick
    public static void MouseDrag(this GameObject obj, ref Vector3 mOffset, ref bool stop)
    {
        if (!stop)
            obj.transform.position = GetMouseWorldPos() + mOffset;
    }

    public static void MouseUp(this GameObject obj, ref Vector3 posStart, ref Quaternion rotStart, ref Vector3 scaleStart, ref bool scaled, ref bool stop)
    {
        if (!stop)
        {
            bool ok = false;
            // put stick into right place if player put it near or back to tools if he does not
            GameObject[] putItemList = GameObject.FindGameObjectsWithTag(tags.placeToPut);

            foreach (GameObject putItemHere in putItemList)
            {
                Vector2 objPos = obj.transform.position;
                Vector2 goalPos = putItemHere.transform.position;
                float adjx = putItemHere.GetComponent<BoxCollider2D>().size.x / 2;
                float adjy = putItemHere.GetComponent<BoxCollider2D>().size.y / 2;
                bool xPosOK = (objPos.x > goalPos.x - adjx - tolerance) && (objPos.x < goalPos.x + adjx + tolerance);
                bool yPosOK = (objPos.y > goalPos.y - adjy - tolerance) && (objPos.y < goalPos.y + adjy + tolerance);

                if (xPosOK && yPosOK && putItemHere.GetComponent<toolHere>().toolsUsed[obj.tag])
                {
                    Vector3 posCopy = obj.transform.position;
                    posCopy.x = putItemHere.transform.position.x;
                    posCopy.y = putItemHere.transform.position.y;
                    posCopy.z = putItemHere.transform.position.z;
                    obj.transform.position = posCopy;

                    ok = true;
                    stop = true;
                    obj.GetComponent<BoxCollider2D>().enabled = false;
                    
                    // run script attached to the object
                    if (obj.GetComponent<toolItem>().scriptName != null)
                        obj.AddComponent(System.Type.GetType(obj.GetComponent<toolItem>().scriptName));

                    // change sorting layer
                    obj.GetComponent<SpriteRenderer>().sortingLayerName = "itemsLayer";
                    if (obj.transform.childCount > 0)
                        obj.transform.GetChild(0).GetComponent<SortingGroup>().sortingLayerName = "itemsLayer";

                    // add game object to the scene
                    obj.transform.parent = null;
                }
            }
            
            if (!ok)
            {
                obj.transform.position = posStart;
                obj.transform.rotation = rotStart;
                obj.transform.localScale = scaleStart;
                scaled = true;
                stop = false;
            }
        }
    }
}
