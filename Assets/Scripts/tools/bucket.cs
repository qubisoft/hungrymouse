﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class bucket : MonoBehaviour
{
    bool onlyOnce = true;

    GameObject bucketAnim;
    private void Update()
    {
        if (onlyOnce)
        {
            if (gameObject.GetComponent<toolItem>().stop)
            {
                bucketAnim = gameObject.transform.GetChild(0).gameObject;
                bucketAnim.transform.GetChild(1).gameObject.SetActive(true);
                bucketAnim.transform.localPosition = new Vector3(-0.08f,-0.4f);
                bucketAnim.GetComponent<Animator>().SetBool("start", true);
                bucketAnim.GetComponent<SortingGroup>().enabled = false;
                onlyOnce = false;
            }
        }
    }


}
