﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class axeStatus : MonoBehaviour
{
    public bool hasStarted = false;
    public bool hasFinished = false;
    public bool inTheMiddle = false;
    public bool sticksAppear = false;
    private bool onlyOnce = true;
    [SerializeField] GameObject smoke;
    [SerializeField] GameObject sticks;
    private GameObject explosion;

    public void starts()
    {
        hasStarted = true;
        Vector3 pos = gameObject.transform.parent.position;
        explosion = Instantiate(smoke, pos + smoke.transform.position, Quaternion.identity);
        explosion.GetComponent<Animator>().SetBool("start", true);
        gameObject.GetComponent<SortingGroup>().sortingOrder = 6;
    }

    public void ends()
    {
        hasFinished = true;
        if (sticksAppear)
        {
            GameObject wood = Instantiate(sticks, transform.position + sticks.transform.position, Quaternion.identity);
            wood.name = "sticks";
            wood.tag = tags.sticks;
        }
        Destroy(explosion);
    }

    public void playSound()
    {
        gameObject.GetComponent<AudioSource>().Play();
    }

    public void middle()
    {
        inTheMiddle = true;
    }
}

