﻿using UnityEngine;

public class flame : MonoBehaviour
{
    public bool backVisible = true;
    bool sticksPut = false;
    bool matchPut = false;
    bool stickPut = false;
    bool axePut = false;
    bool onlyOnce = true;
    GameObject back;
    Color backCol;
    private void Start()
    {
        back = gameObject.transform.GetChild(2).gameObject;
        backCol = back.GetComponent<SpriteRenderer>().color;
        if (!backVisible)
        {
            Color backColTrans = backCol;
            backColTrans.a = 0f;
            back.GetComponent<SpriteRenderer>().color = backColTrans;
        }
    }
    private void Update()
    {
        if (onlyOnce)
        {
            GameObject match = GameObject.FindGameObjectWithTag(tags.match);
            GameObject sticks = GameObject.FindGameObjectWithTag(tags.sticks);
            GameObject stick = GameObject.FindGameObjectWithTag(tags.stick);
            GameObject axe = GameObject.FindGameObjectWithTag(tags.axe);

            if (!matchPut)
                matchPut = (match != null && match.GetComponent<toolItem>().stop);

            if (!stickPut)
                stickPut = stick != null && stick.GetComponent<toolItem>().stop;

            if (!axePut)
                axePut = axe != null && axe.GetComponent<toolItem>().stop;

            if (!sticksPut)
                sticksPut = (sticks != null && sticks.GetComponent<toolItem>().stop) || (stickPut && axePut);
  
            if (sticksPut)
            {
                if (!backVisible)
                    back.GetComponent<SpriteRenderer>().color = backCol;
                Destroy(sticks);
            }

            if (sticksPut && matchPut)
            {
                gameObject.GetComponent<Animator>().SetBool("start", true);
                gameObject.GetComponent<Animator>().SetBool("continue", true);
                onlyOnce = false;
                Destroy(match);
            }
        }
    }
}
