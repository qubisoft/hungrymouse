﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stick : MonoBehaviour
{
    private void Update()
    {
        GameObject axe = GameObject.FindGameObjectWithTag(tags.axe);
        if (axe != null)
        {
            axe.transform.GetChild(0).GetComponent<axeStatus>().sticksAppear = true;
            if (axe.transform.GetChild(0).GetComponent<axeStatus>().hasFinished)
                Destroy(gameObject);
        }
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == tags.axe)
    //        collision.gameObject.transform.GetChild(0).GetComponent<Animator>().SetBool("start", true);
    //}
}
