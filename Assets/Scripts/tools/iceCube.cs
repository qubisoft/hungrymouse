﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iceCube : MonoBehaviour
{
    bool matchPut = false;
    bool stickPut = false;
    bool axePut = false;
    bool sticksPut = false;
    bool onlyOnce = true;
    GameObject cheese;

    private void Start()
    {
        cheese = GameObject.FindGameObjectWithTag(tags.cheese);
    }
    private void Update()
    {
        GameObject match = GameObject.FindGameObjectWithTag(tags.match);
        GameObject sticks = GameObject.FindGameObjectWithTag(tags.sticks);
        GameObject stick = GameObject.FindGameObjectWithTag(tags.stick);
        GameObject axe = GameObject.FindGameObjectWithTag(tags.axe);

        if (!matchPut)
            matchPut = match != null && match.GetComponent<toolItem>().stop;

        if (!stickPut)
            stickPut = stick != null && stick.GetComponent<toolItem>().stop;

        if (!axePut)
            axePut = axe != null && axe.GetComponent<toolItem>().stop;

        if (!sticksPut)
            sticksPut = (sticks != null && sticks.GetComponent<toolItem>().stop) || (stickPut && axePut);

        if (onlyOnce && matchPut && sticksPut)
        {
            gameObject.GetComponent<Animator>().SetBool("start", true);
            gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
            match.GetComponent<PolygonCollider2D>().isTrigger = true;
            Color cheeseColor = cheese.GetComponent<SpriteRenderer>().color;
            cheeseColor.a = 1f;
            cheese.GetComponent<SpriteRenderer>().color = cheeseColor;
            onlyOnce = false;
        }
    }
}
