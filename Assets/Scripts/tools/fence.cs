﻿using UnityEngine;

public class fence : MonoBehaviour
{
    private bool go = false;
    private bool onlyOnce = true;

    private GameObject player;
    private GameObject player2;
    private GameObject target;
    private GameObject ground;

    private GameObject temp1;
    private GameObject temp2;

    private Vector3 tempTarget1;
    private Vector3 tempTarget2;

    private bool target1get = false;
    private bool target2get = false;

    private void Start()
    {
        // create vectors for new target points to omit fence
        float fenceWidth = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float fenceHeight = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        Vector3 fencePos = gameObject.transform.position;
        tempTarget1 = new Vector3(fencePos.x - fenceWidth / 2, fencePos.y - fenceHeight, fencePos.z);
        tempTarget2 = new Vector3(fencePos.x + fenceWidth / 2, fencePos.y - fenceHeight, fencePos.z);
        temp1 = new GameObject("temp1");
        temp2 = new GameObject("temp2");

        // find ground
        ground = GameObject.FindGameObjectWithTag(tags.ground);
    }
    private void Update()
    {
        if (go)
        {
            // if target1 reached, go to target2
            if (target1get)
            {
                setTarget(player, temp2);
                // if target2 reached, back to the old one
                if (target2get)
                {
                    go = false;
                    setTarget(player, target);
                }
                else
                    target2get = targetReached(player, temp2);
            }
            else
                target1get = targetReached(player, temp1);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;
        if (onlyOnce && (tag == tags.mouseRuns || tag == tags.enemyRuns))
        {
            onlyOnce = false;
            
            player = collision.gameObject;
            target = getTarget(player);

            if (tag == tags.mouseRuns && GameObject.FindGameObjectWithTag(tags.enemyRuns) != null)
                player2 = GameObject.FindGameObjectWithTag(tags.enemyRuns).gameObject;
            if (tag == tags.enemyRuns)
                player2 = GameObject.FindGameObjectWithTag(tags.mouseRuns).gameObject;

            // mouse has to go to the ground
            player.GetComponent<Rigidbody2D>().gravityScale = 0;
            if (player2 != null)
                player2.GetComponent<Rigidbody2D>().gravityScale = 0;
            ground.GetComponent<PolygonCollider2D>().enabled = false;

            // if player is on the right, switch temp1 and temp2
            if (player.transform.position.x <= gameObject.transform.position.x)
            {
                temp1.transform.position = tempTarget1;
                temp2.transform.position = tempTarget2;
            }
            else
            {
                temp1.transform.position = tempTarget2;
                temp2.transform.position = tempTarget1;
            }

            // go down the fence
            setTarget(player, temp1);
            go = true;
        }
    }

    private bool targetReached(GameObject obj, GameObject target)
    {
        float posXobj1 = target.transform.position.x;
        float posYobj1 = target.transform.position.y;
        float posXobj2 = obj.transform.position.x;
        float posYobj2 = obj.transform.position.y;
        float deltaX = System.Math.Abs(posXobj1 - posXobj2);
        float deltaY = System.Math.Abs(posYobj1 - posYobj2);
        if (System.Math.Round(deltaX, 2) == 0 && System.Math.Round(deltaY, 2) == 0)
            return true;
        else
            return false;
    }

    private GameObject getTarget(GameObject obj)
    {
        if (obj.tag == tags.mouseRuns)
            return obj.GetComponent<mouseRunningScript>().cheese;
        else if (obj.tag == tags.enemyRuns)
            return obj.GetComponent<enemyRuns>().enemyTarget;
        else
            return null;
    }

    private void setTarget(GameObject obj, GameObject target)
    {
        if (obj.tag == tags.mouseRuns)
            obj.GetComponent<mouseRunningScript>().cheese = target;
        else if (obj.tag == tags.enemyRuns)
            obj.GetComponent<enemyRuns>().enemyTarget = target;
    }
}
