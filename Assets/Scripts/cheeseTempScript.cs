﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cheeseTempScript : MonoBehaviour
{
    private Vector3 pos;
    private bool onlyOnce = true;
    private bool start = false;

    private void Start()
    {
        pos = gameObject.transform.position;
        pos.y -= 2.4f;
        
    }
    private void Update()
    {
        if (start)
            transform.position = Vector3.MoveTowards(transform.position, pos, staticClass.speed * Time.deltaTime);

        if (onlyOnce)
        {
            GameObject axe = GameObject.FindGameObjectWithTag(tags.axe);

            if (axe != null && axe.transform.GetChild(0).GetComponent<axeStatus>().inTheMiddle)
            {
                onlyOnce = false;
                start = true;
                GameObject treeCollider = GameObject.FindGameObjectWithTag(tags.treeTop);
                treeCollider.GetComponent<PolygonCollider2D>().isTrigger = true;
            }
        }
    }
}
