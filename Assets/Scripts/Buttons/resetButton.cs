﻿using UnityEngine;

public class resetButton : MonoBehaviour
{
    [SerializeField] GameObject questionTab;

    private void OnMouseDown()
    {
        // show question tab
        GameObject tab = Instantiate(questionTab, new Vector3(0, 0,-5), Quaternion.identity);
        tab.name = "questionTab";
    }
}
