﻿using UnityEngine;

public class levelButton : MonoBehaviour
{
    public int levelNumber;
    public bool lastOne = false;
    private void OnMouseDown()
    {
        if (!lastOne)
            sceneNavigator.sceneNavigation("Level" + levelNumber);
        else
            sceneNavigator.sceneNavigation("Final");
    }
}
