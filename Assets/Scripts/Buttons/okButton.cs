﻿using UnityEngine;

public class okButton : MonoBehaviour
{
    private void OnMouseDown()
    {
        // reset level reached
        for (int levelIndex = 1; levelIndex < PlayerPrefs.GetInt(staticClass.levelKey); levelIndex++)
            PlayerPrefs.DeleteKey(staticClass.starKey + levelIndex);

        PlayerPrefs.DeleteKey(staticClass.levelKey);
        PlayerPrefs.DeleteKey(staticClass.toolChildKey);
        PlayerPrefs.DeleteKey(staticClass.toolKey);

        // back to the list
        sceneNavigator.sceneNavigation("List");
    }
}
