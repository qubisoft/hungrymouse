﻿using UnityEngine;

public class okPauseButton : MonoBehaviour
{
    private void OnMouseDown()
    {
        Time.timeScale = 1f;
        GameObject win = gameObject.transform.parent.gameObject;
        Destroy(win);
    }
}
