﻿using UnityEngine;

public class pauseButton : MonoBehaviour
{
    [SerializeField] GameObject popup;

    private void OnMouseDown()
    {
        GameObject win = Instantiate(popup, new Vector3(0, -1, -5), Quaternion.identity);
        win.name = "pauseWindow";
        Time.timeScale = 0;
    }
}
