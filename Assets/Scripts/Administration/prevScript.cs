﻿using UnityEngine;

public class prevScript : MonoBehaviour
{
    [SerializeField] GameObject levelNavigation;
    [SerializeField] string direction;

    private void OnMouseDown()
    {
        levels lev = levelNavigation.GetComponent<levels>();
        int tabNum = lev.currTab;
        
        switch (direction)
        {
            case "prev":
                tabNum -= 1;
                if (tabNum > 0)
                {
                    lev.currTab = tabNum;
                    lev.setLevels(lev.currTab);
                }
                break;
            case "next":
                tabNum += 1;
                if (tabNum <= lev.tabs.Length)
                {
                    lev.currTab = tabNum;
                    lev.setLevels(lev.currTab);
                }
                break;
        }

    }    
}
