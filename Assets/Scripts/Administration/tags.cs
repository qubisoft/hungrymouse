﻿public static class tags 
{
    // mouse
    public const string mouseSleeps = "tagMouseSleeps";
    public const string mouseRuns = "tagMouseRuns";
    public const string mouseEats = "tagMouseEats";

    // enemy
    public const string enemy = "tagEnemy";
    public const string enemySleeps = "tagEnemySleeps";
    public const string enemyRuns = "tagEnemyRuns";
    public const string enemyEats = "tagEnemyEats";
    public const string enemyTrapped = "tagEnemyTrapped";
    public const string iceCube = "tagIceCube";

    // objects
    public const string cheese = "tagCheese";
    public const string hand = "tagHand";
    public const string cactus = "tagCactus";
    public const string placeToPut = "tagPlaceToPut";
    public const string hole = "tagHole";
    public const string ground = "tagGround";
    public const string toolBoxes = "tagToolBoxes";
    public const string river = "tagRiver";
    public const string flame = "tagFlame";

    // panel
    public const string timer = "tagTimer";
    public const string soundSlider = "tagSoundSlider";
    public const string timeSlider = "tagTimeSlider";

    // tools
    public const string stick = "tagStick"; 
    public const string fence = "tagFence";
    public const string jumper = "tagJumper";
    public const string wool = "tagWool";
    public const string cage = "tagCage";
    public const string axe = "tagAxe";
    public const string treeToCut = "tagTreeToCut";
    public const string treeTop = "tagTreeTop";
    public const string sticks = "tagSticks";
    public const string match = "tagMatch";
    public const string bucket = "tagBucket";
}
