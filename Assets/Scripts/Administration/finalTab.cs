﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finalTab : MonoBehaviour
{
    [SerializeField] GameObject[] levels;
    [SerializeField] Sprite[] stars;

    private void Start()
    {
        gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);
        for (int i = 0; i < levels.Length; i++)
        {
            int levelNo = i + 1;
            int starNo = PlayerPrefs.GetInt(staticClass.starKey + levelNo);
            levels[i].transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = stars[starNo-1];
        }
    }
}
