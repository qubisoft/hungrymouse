﻿using UnityEngine;

public class toolsBottomPanel : MonoBehaviour
{
    private int toolsUnlockedTotal = 0;
    private int numberOfItemsInOneBox = 3;

    [SerializeField] toolItemScriptableObject[] item;
    [SerializeField] GameObject[] toolBox;

    public int boxActive;

    private void Start()
    {
        // number of unlocked tools
        if (PlayerPrefs.HasKey(staticClass.toolChildKey))
            toolsUnlockedTotal = PlayerPrefs.GetInt(staticClass.toolChildKey);
        
        // we have three tool boxes
        for (int i = 0; i < toolBox.Length; i++)
        {
            // each tool box contains three items
            for (int j = numberOfItemsInOneBox * i; j < System.Math.Min(toolsUnlockedTotal, numberOfItemsInOneBox * (i+1)); j++)
            {
                GameObject toolItem = toolBox[i].transform.GetChild(0).GetChild(j % numberOfItemsInOneBox).gameObject;
                
                toolItem.GetComponent<SpriteRenderer>().sprite = item[j].img;
                toolItem.AddComponent<PolygonCollider2D>();
                toolItem.AddComponent<Rigidbody2D>();
                toolItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                toolItem.GetComponent<Rigidbody2D>().useFullKinematicContacts = true;
                toolItem.AddComponent<toolItem>();
                toolItem.tag = item[j].tag;
                toolItem.transform.localScale *= item[j].scale;
 
                if (item[j].anim != null)
                {
                    // if we have animation, we don't need sprite image
                    toolItem.GetComponent<SpriteRenderer>().sprite = null;
                    GameObject animObject = Instantiate(item[j].anim);
                    animObject.name = "anim";
                    animObject.transform.parent = toolItem.transform;
                    animObject.transform.localScale = new Vector3(1,1,1);
                    animObject.transform.localPosition = item[j].anim.transform.position; 
                    animObject.GetComponent<Animator>().SetBool("start", false);
                }

                if (item[j].script != null)
                    toolItem.GetComponent<toolItem>().scriptName = item[j].script;

                // if cage, change polygon collider manually to player can go into it
                if (item[j].tag == tags.cage)
                {
                    Vector2[] colliderPoints = new Vector2[4];
                    colliderPoints[0] = new Vector2(-0.5f, -1f);
                    colliderPoints[1] = new Vector2(-0.5f, 0f);
                    colliderPoints[2] = new Vector2(0f, 0f);
                    colliderPoints[3] = new Vector2(0f, -1f);
                    toolItem.GetComponent<PolygonCollider2D>().points = colliderPoints;
                }
            }
            toolBox[i].SetActive(false);
        }

        // set active the last item box
        if (boxActive == 0)
            toolBox[(toolsUnlockedTotal - 1) / numberOfItemsInOneBox].SetActive(true);
        else
            toolBox[boxActive - 1].SetActive(true);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            for (int i = 0; i < toolBox.Length; i++)
            {
                // prev is clicked
                if (toolBox[i].transform.GetChild(1).GetComponent<isClicked>().isClickedFlag)
                {
                    toolBox[i].SetActive(false);
                    toolBox[i - 1].SetActive(true);
                }
                // next is clicked
                if (toolBox[i].transform.GetChild(2).GetComponent<isClicked>().isClickedFlag)
                {
                    toolBox[i].SetActive(false);
                    toolBox[i + 1].SetActive(true);
                }
                // buttons unclicked
                toolBox[i].transform.GetChild(1).GetComponent<isClicked>().isClickedFlag = false;
                toolBox[i].transform.GetChild(2).GetComponent<isClicked>().isClickedFlag = false;
            }
        }
    }
}
