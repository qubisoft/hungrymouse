﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class toolsUnlocked : MonoBehaviour
{
    [SerializeField] GameObject toolWin;
    [SerializeField] Sprite[] tool;

    private int levelNumber;
    private bool run;
    private List<int> levels = new List<int> { 1, 2, 3, 4, 5, 7, 9, 10, 13};   // number of levels with new tools to unlock

    void Start()
    {
        string levelName = SceneManager.GetActiveScene().name;
        levelNumber = System.Int32.Parse(levelName.Substring("Level".Length));

        if (!PlayerPrefs.HasKey(staticClass.toolKey))
        {
            PlayerPrefs.SetInt(staticClass.toolKey, 0);
            PlayerPrefs.SetInt(staticClass.toolChildKey, 0);
            PlayerPrefs.Save();
        }

        if (levels.Contains(levelNumber) && PlayerPrefs.GetInt(staticClass.toolKey) < levelNumber)         
        {
            PlayerPrefs.SetInt(staticClass.toolKey, levelNumber);
            PlayerPrefs.SetInt(staticClass.toolChildKey, PlayerPrefs.GetInt(staticClass.toolChildKey) + 1);
            PlayerPrefs.Save();
            run = true;
        }
    }

    public void showWindow()
    {
        if (run)
        {
            PlayerPrefs.SetInt(staticClass.toolKey + levelNumber, 1);
            GameObject toolWindow = Instantiate(toolWin, new Vector3(0, 0, -6f), Quaternion.identity);
            toolWindow.transform.Find("tools").GetComponent<SpriteRenderer>().sprite = tool[PlayerPrefs.GetInt(staticClass.toolChildKey)-1];
        }
    }
}
