﻿using UnityEngine;
using UnityEngine.UI;

public class minusSign : MonoBehaviour
{
    private void OnMouseDown()
    {
        Slider slider = gameObject.transform.parent.GetComponent<Slider>();
        sliderSettings sliderScript = gameObject.transform.parent.GetComponent<sliderSettings>();

        if (slider.wholeNumbers)
            slider.value -= 1;
        else
            slider.value -= 0.1f;

        sliderScript.setValue(sliderScript.type, slider.value);
    }
}
