﻿using UnityEngine;
using UnityEngine.UI;

public class levels: MonoBehaviour
{      
    private string buttonName = "button";
    public GameObject[] tabs;
    public GameObject[] dots;
    [SerializeField] Sprite star0;
    [SerializeField] Sprite star1;
    [SerializeField] Sprite star2;
    [SerializeField] Sprite star3;
    public int childrenInOneTab;
    public int currTab;

    void Start()
    {
        // set level 1 if first time
        if (!PlayerPrefs.HasKey(staticClass.levelKey))
        {
            PlayerPrefs.SetInt(staticClass.levelKey, 1);
            PlayerPrefs.Save();
        }

        // read level reached
        int levelReached = PlayerPrefs.GetInt(staticClass.levelKey);

        // show current tab
        currTab = (levelReached - 1) / childrenInOneTab + 1;

        setLevels(currTab);
    }

    public void setLevels(int currTab)
    {
        // read level reached
        int levelReached = PlayerPrefs.GetInt(staticClass.levelKey);

        //tab1: from 1 to 9, tab2: from 10 to 18
        GameObject tab = tabs[currTab - 1];
        tab.SetActive(true);

        // colour current dot
        for (int i = 1; i <= dots.Length; i++)
        {
            if (i == currTab)
                selectDot(dots[i - 1], true);
            else
                selectDot(dots[i - 1], false);
        }

        // loop through tabs and hide the rest
        for (int i = 1; i <= tabs.Length; i++)
        {
            if (i != currTab)
                tabs[i - 1].SetActive(false);
        }

        for (int i = 1; i <= tab.transform.childCount; i++)
        {
            GameObject levelButton = tab.transform.GetChild(i-1).gameObject;
            bool unlocked = false;
            if (i + childrenInOneTab*(currTab-1) <= levelReached)
                unlocked = true;

            unlockButton(levelButton, unlocked);
        }
    }

    public void selectDot(GameObject dot, bool select)
    {
        dot.transform.GetChild(0).gameObject.SetActive(select);
        dot.transform.GetChild(1).gameObject.SetActive(!select);
    }

    public void unlockButton(GameObject levelButton, bool unlocked)
    {
        // show number of this level
        GameObject text = levelButton.transform.GetChild(0).gameObject;
        text.SetActive(unlocked);

        // hide lock icon
        GameObject symbol = levelButton.transform.GetChild(1).gameObject;
        symbol.SetActive(!unlocked);

        if (unlocked)
        {
            // scene navigation
            int levelIndex = System.Int32.Parse(levelButton.name.Substring(buttonName.Length));
            levelButton.GetComponent<levelButton>().levelNumber = levelIndex;
            // stars                   
            SpriteRenderer stars = levelButton.transform.GetChild(2).GetComponent<SpriteRenderer>();
            switch (PlayerPrefs.GetInt(staticClass.starKey + levelIndex))
            {
                case 0:
                    stars.sprite = star0;
                    break;
                case 1:
                    stars.sprite = star1;
                    break;
                case 2:
                    stars.sprite = star2;
                    break;
                case 3:
                    stars.sprite = star3;
                    break;
                default:
                    stars.sprite = star0;
                    break;
            }
        }

        // unlock the button
        levelButton.GetComponent<Button>().interactable = unlocked;
        levelButton.GetComponent<BoxCollider2D>().enabled = unlocked;
    }
}
