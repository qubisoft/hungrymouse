﻿using UnityEngine;
using UnityEngine.UI;

public class sliderSettings : MonoBehaviour
{
    public string type;
    [SerializeField] Text textToFill;
    [SerializeField] GameObject bg;

    private void Start()
    {
        string key = "";

        switch (type)
        {
            case "sound":
                key = staticClass.soundKey;
                break;
            case "time":
                key = staticClass.timeKey;
                break;
        }

        // read settings if exist or set initial values if don't
        if (PlayerPrefs.HasKey(key))
            gameObject.GetComponent<Slider>().value = PlayerPrefs.GetFloat(key);
        else
        {
            gameObject.GetComponent<Slider>().value = 1;
            PlayerPrefs.SetFloat(key, 1);
            PlayerPrefs.Save();
        }

        setValue(type, PlayerPrefs.GetFloat(key));
    }

    // settings after slider change
    public void setValue(string type, float value)
    {
        switch (type)
        {
            case "sound":
                PlayerPrefs.SetFloat(staticClass.soundKey, value);
                textToFill.text = (100 * value).ToString("F0");
                bg.GetComponent<AudioSource>().volume = 0.8f * PlayerPrefs.GetInt(staticClass.musicKey) * PlayerPrefs.GetFloat(staticClass.soundKey);
                break;
            case "time":
                PlayerPrefs.SetFloat(staticClass.timeKey, value);
                switch (value)
                {
                    case 0:
                        textToFill.text = "None";
                        break;
                    case 1:
                        textToFill.text = "Easy";
                        break;
                    case 2:
                        textToFill.text = "Hard";
                        break;
                }
                break;
        }
        PlayerPrefs.Save();
    }
}
