﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundMusic : MonoBehaviour
{
    void Start()
    {
        if (!PlayerPrefs.HasKey(staticClass.musicKey))
        {
            PlayerPrefs.SetInt(staticClass.musicKey, 1);
            PlayerPrefs.Save();
        }

        if (!PlayerPrefs.HasKey(staticClass.soundKey))
        {
            PlayerPrefs.SetFloat(staticClass.soundKey, 0.3f);
            PlayerPrefs.Save();
        }

        gameObject.GetComponent<AudioSource>().volume = 0.8f * PlayerPrefs.GetInt(staticClass.musicKey) * PlayerPrefs.GetFloat(staticClass.soundKey);
    }
}
