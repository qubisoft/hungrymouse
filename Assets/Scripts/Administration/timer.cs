﻿using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    public float timeTarget;
    public Text timeText;
    public GameObject fillImg;
    public GameObject clock;
    public bool hasStarted = true;
    private bool noTime;

    private float timeStart;
    private float time;

    GameObject star1;
    GameObject star2;
    GameObject star3;

    void Start()
    {
        // read time settings and set initial values
        switch (PlayerPrefs.GetFloat(staticClass.timeKey))
        {
            case 0:
                //gameObject.SetActive(false);
                noTime = true;
                break;
            case 1:
                timeStart = timeTarget;
                time = timeTarget;
                noTime = false;
                break;
            case 2:
                timeStart = timeTarget/2;
                time = timeTarget/2;
                noTime = false;
                break;
        }

        // all stars shown
        star1 = gameObject.transform.Find("star_1").gameObject;
        star2 = gameObject.transform.Find("star_2").gameObject;
        star3 = gameObject.transform.Find("star_3").gameObject;

        // set three stars at the begginning
        PlayerPrefs.SetInt(staticClass.starKey, 3);
        PlayerPrefs.Save();
    }

    void Update()
    {
        if (hasStarted && !noTime)
            timeRun();
    }

    private void timeRun()
    {
        // show time number
        time -= Time.deltaTime;
        fillImg.GetComponent<Image>().fillAmount = time / timeStart;
        timeText.text = time.ToString("F0");

        // if time is close to end run clock animation
        if (System.Math.Round(time-3.5,2) == 0)
            clock.GetComponent<Animator>().SetTrigger("runOutOfTime");

        // if time is close to end, make timer red
        if (time <= 3)
            timeText.color = new Color32(255, 0, 0, 255);

        // start the mouse if time is out
        if (time < 0)
        {
            GameObject mouse = GameObject.FindGameObjectWithTag(tags.mouseSleeps);
            mouse.GetComponent<mouseSleepingScript>().OnMouseDown();
            hasStarted = false;
            star1.GetComponent<Animator>().SetTrigger("Active");
        }

        // reduce number of received stars
        if (fillImg.GetComponent<Image>().fillAmount < 0.5)
        {
            //star3.SetActive(false);
            star3.GetComponent<Animator>().SetTrigger("Active");
            PlayerPrefs.SetInt(staticClass.starKey, 2);
            PlayerPrefs.Save();
        }
        if (fillImg.GetComponent<Image>().fillAmount < 0.15)
        {
            //star2.SetActive(false);
            star2.GetComponent<Animator>().SetTrigger("Active");
            PlayerPrefs.SetInt(staticClass.starKey, 1);
            PlayerPrefs.Save();
        }
    }
}
