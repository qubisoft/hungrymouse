﻿using UnityEngine.SceneManagement;

public static class sceneNavigator
{
    public static void sceneNavigation(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
