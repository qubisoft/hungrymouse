﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infoNavigation : MonoBehaviour
{
    [SerializeField] GameObject[] tabs;

    public void setTabNo(int tabNo)
    {
        if (tabNo == 0)
            tabNo = 1;

        for (int i = 0; i < tabs.Length; i++)
            {
                if (tabNo == i + 1)
                    tabs[i].SetActive(true);
                else
                    tabs[i].SetActive(false);
            }
    }
}
