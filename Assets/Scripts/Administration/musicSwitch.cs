﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class musicSwitch : MonoBehaviour
{
    [SerializeField] Sprite musicOn;
    [SerializeField] Sprite musicOff;
    [SerializeField] GameObject bg;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey(staticClass.musicKey))
        {
            PlayerPrefs.SetInt(staticClass.musicKey, 1);
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.GetInt(staticClass.musicKey) == 0)
            gameObject.GetComponent<SpriteRenderer>().sprite = musicOff;
        else
            gameObject.GetComponent<SpriteRenderer>().sprite = musicOn;
    }

    private void OnMouseDown()
    {
        if (PlayerPrefs.GetInt(staticClass.musicKey) == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = musicOn;
            PlayerPrefs.SetInt(staticClass.musicKey, 1);
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = musicOff;
            PlayerPrefs.SetInt(staticClass.musicKey, 0);
        }
        PlayerPrefs.Save();
        bg.GetComponent<AudioSource>().volume = 0.8f * PlayerPrefs.GetInt(staticClass.musicKey) * PlayerPrefs.GetFloat(staticClass.soundKey);
    }
}
