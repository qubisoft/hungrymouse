﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class prepareWinWindow : MonoBehaviour
{
    [SerializeField] Sprite star1;
    [SerializeField] Sprite star2;
    [SerializeField] Sprite star3;

    void Start()
    {
        // find number of current level
        string levelName = SceneManager.GetActiveScene().name;
        int levelIndex = System.Int32.Parse(levelName.Substring("Level".Length));

        // unlock next level it current is the last one unlocked
        if (levelIndex == PlayerPrefs.GetInt(staticClass.levelKey))
        {
            PlayerPrefs.SetInt(staticClass.levelKey, levelIndex + 1);
            PlayerPrefs.Save();
        }

        // set next level button
        if (levelIndex < 15)
            gameObject.transform.Find("nextButton").GetComponent<levelButton>().levelNumber = levelIndex + 1;
        else
            gameObject.transform.Find("nextButton").GetComponent<levelButton>().lastOne = true;

        // show number of received stars and save record on level
        SpriteRenderer starImage = gameObject.transform.Find("table").Find("stars").GetComponent<SpriteRenderer>();
        switch (PlayerPrefs.GetInt(staticClass.starKey))
        {
            case 1:
                starImage.sprite = star1;
                break;
            case 2:
                starImage.sprite = star2;
                break;
            case 3:
                starImage.sprite = star3;
                break;
            default:
                starImage.sprite = null;
                break;
        }

        int starCurrent = 0;
        if (PlayerPrefs.HasKey(staticClass.starKey + levelIndex))
        {
            starCurrent = PlayerPrefs.GetInt(staticClass.starKey + levelIndex);
        }
        int starNew = PlayerPrefs.GetInt(staticClass.starKey);
        if (starNew > starCurrent)
        {
            PlayerPrefs.SetInt(staticClass.starKey + levelIndex, PlayerPrefs.GetInt(staticClass.starKey));
            PlayerPrefs.Save();
        }
    }
}
