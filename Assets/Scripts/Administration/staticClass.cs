﻿public static class staticClass 
{
    // player prefs
    public const string timeKey = "timeLimits";
    public const string soundKey = "soundVolume";
    public const string musicKey = "musicOn";
    public const string levelKey = "levelReached";
    public const string starKey = "starsReached";
    public const string toolKey = "toolsUnlocked";
    public const string toolChildKey = "toolsChild";

    // level result
    public static bool levelFinished;

    // speed, height
    public static float speed = 3f;
    public static float enemySpeed = 4f;
    public static float height = 14.5f;
}
