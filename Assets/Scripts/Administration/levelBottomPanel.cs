﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class levelBottomPanel : MonoBehaviour
{
    [SerializeField] Sprite zero;
    [SerializeField] Sprite one;
    [SerializeField] Sprite two;
    [SerializeField] Sprite three;
    [SerializeField] Sprite four;
    [SerializeField] Sprite five;
    [SerializeField] Sprite six;
    [SerializeField] Sprite seven;
    [SerializeField] Sprite eight;
    [SerializeField] Sprite nine;

    // Start is called before the first frame update
    void Start()
    {
        int len = "Level".Length;
        int firstNumber;
        int secondNumber;
        GameObject num1 = gameObject.transform.GetChild(0).gameObject;
        GameObject num2 = gameObject.transform.GetChild(1).gameObject;
        num1.SetActive(true);
        num1.SetActive(true);

        string levelName = SceneManager.GetActiveScene().name;
        if (levelName.Length == len + 1)     // level number from 1 to 9
        {
            num1.SetActive(false);
            num2.GetComponent<SpriteRenderer>().sprite = numberToSprite(System.Int32.Parse(levelName.Substring(len)));
        }
        else     // levels from 10
        {
            num1.GetComponent<SpriteRenderer>().sprite = numberToSprite(System.Int32.Parse(levelName.Substring(len, 1)));
            num2.GetComponent<SpriteRenderer>().sprite = numberToSprite(System.Int32.Parse(levelName.Substring(len + 1)));
        }
    }

    private Sprite numberToSprite(int number)
    {
        switch (number)
        {
            case 0:
                return zero;
            case 1:
                return one;
            case 2:
                return two;
            case 3:
                return three;
            case 4:
                return four;
            case 5:
                return five;
            case 6:
                return six;
            case 7:
                return seven;
            case 8:
                return eight;
            case 9:
                return nine;
            default:
                return zero;
        }
    }
}