﻿using UnityEngine;

public class mouseSleepingScript : MonoBehaviour
{
    [SerializeField] GameObject mouseRunning;
    public bool isClicked = false;

    private GameObject[] enemy;
    private enemyScriptableObject enemySO;
    private enemyScript enemyScript;
    private int enemiesCount;

    public void OnMouseDown()
    {
        isClicked = true;

        // find cheese and enemy if exists and sleeps
        GameObject cheese = GameObject.FindGameObjectWithTag(tags.cheese);
        enemy = GameObject.FindGameObjectsWithTag(tags.enemy);
        enemiesCount = enemy.Length;

        // wake up
        GameObject mouseRuns = Instantiate(mouseRunning, transform.position, transform.rotation);
        mouseRuns.GetComponent<mouseRunningScript>().cheese = cheese;
        mouseRuns.name = "mouseRunning";
        
        // wake up enemy if exists in level
        if (enemiesCount > 0)
        {
            for (int i = 0; i < enemiesCount; i++)
            {
                enemyScript = enemy[i].GetComponent<enemyScript>();
                if (enemyScript.enemyStatus == "sleeps")
                {
                    if (enemyScript.enemySO.isMouse)
                        enemyScript.enemyTarget = cheese;
                    else
                        enemyScript.enemyTarget = mouseRuns;

                    enemyScript.wakeUp();
                }
            }
        }

        // timer stops
        if (PlayerPrefs.GetFloat(staticClass.timeKey) > 0)
        {
            GameObject timer = GameObject.FindGameObjectWithTag(tags.timer);
            timer.GetComponent<timer>().hasStarted = false;
        }

        Destroy(gameObject);
    }
}
