﻿using UnityEngine;

public class enemyRuns : MonoBehaviour
{
    public GameObject enemy;
    public GameObject enemyTarget;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        enemyScript enemyScript = enemy.GetComponent<enemyScript>();
        enemyScriptableObject enemySO = enemyScript.enemySO;

        string tag = collision.gameObject.tag;

        // enemy other than mouse - eat mouse
        if (!enemySO.isMouse && tag == tags.mouseRuns)
            enemyScript.eat();

        // enemy brown mouse - eat cheese
        if (enemySO.isMouse && tag == tags.cheese)
        {
            enemyScript.eat();
            collision.gameObject.SetActive(false);
        }

        // enemy trapped by wool or cage if affects
        if ((tag == tags.wool && enemySO.woolReact) || (tag == tags.cage && enemySO.cageReact) || (tag == tags.treeTop && enemySO.treeReact))
            enemyScript.trap();
    }

    void Update()
    {
        Vector3 pos = enemyTarget.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, pos, staticClass.enemySpeed * Time.deltaTime);
    }
}
