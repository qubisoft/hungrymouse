﻿using UnityEngine;

public class hideHand1 : MonoBehaviour
{
    // hide tip hand if mouse runs
    private GameObject mouse;

    private void Start()
    {
        mouse = GameObject.FindGameObjectWithTag(tags.mouseSleeps);
    }

    private void Update()
    {
        if (mouse.GetComponent<mouseSleepingScript>().isClicked)
            gameObject.SetActive(false);
    }
}
