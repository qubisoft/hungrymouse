﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class mouseRunningScript : MonoBehaviour
{
    [SerializeField] GameObject mouseEating;
    [SerializeField] GameObject lostWindow;
    [SerializeField] GameObject winWindow;

    public GameObject cheese;

    private void Start()
    {
        // play running music
        gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);
    }

    void Update()
    {
        if (cheese != null)
        {
            // turn mouse to target and move towards it
            if (cheese.transform.position.x < transform.position.x)
            {
                Quaternion rot = gameObject.transform.rotation;
                rot.y += 180;
                gameObject.transform.rotation = rot;
            }
            transform.position = Vector3.MoveTowards(transform.position, cheese.transform.position, staticClass.speed * Time.deltaTime);
        }
        else
        {
            // show lost window
            GameObject result = Instantiate(lostWindow, new Vector3(0, -1f, -5), Quaternion.identity);
            result.name = "lostWindow";
            result.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);

            // stop mouse
            destroyIt();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string colTag = collision.gameObject.tag;

        //win
        if (colTag == tags.cheese)
        {
            Vector3 cheesePos = cheese.transform.position;

            // hide cheese
            cheese.SetActive(false);

            GameObject mouseEats = Instantiate(mouseEating, cheesePos + mouseEating.transform.position, Quaternion.identity);
            mouseEats.name = "mouseEats";

            // Show win window
            GameObject result = Instantiate(winWindow, new Vector3(0, -1f, -5), Quaternion.identity);
            result.name = "winWindow";
            result.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);                

            // stop mouse
            destroyIt();
        }

        //fail
        if (colTag == tags.cactus || colTag == tags.hole || colTag == tags.enemyEats || colTag == tags.iceCube || colTag == tags.flame)
        {
            // show lost window
            GameObject result = Instantiate(lostWindow, new Vector3(0, -1f, -5), Quaternion.identity);
            result.name = "lostWindow";
            result.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(staticClass.soundKey);

            // stop mouse
            destroyIt();
        }
    }

    private void destroyIt()
    {
        Destroy(gameObject);
    }
}


