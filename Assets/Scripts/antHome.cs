﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class antHome : MonoBehaviour
{
    [SerializeField] GameObject ant;
    private GameObject mouse;
    private GameObject cheese;
    private GameObject targetTemp;
    private GameObject[] ants = new GameObject[3];          // ant0, ant1, ant2
    private Vector3 firstPos;
    private Vector3 middlePos;
    private Vector3 lastPos;
    private bool cheeseMove = false;
    private bool start = true;
    private bool[] antGo = new bool[3];                     // if ant (0, 1 or 2) starts going
    private Vector3[] currTarget = new Vector3[3];          // current target (first or last pos) for each ant

    private void Start()
    {
        // initial variables
        mouse = GameObject.FindGameObjectWithTag(tags.mouseSleeps);
        cheese = GameObject.FindGameObjectWithTag(tags.cheese);
        targetTemp = gameObject.transform.Find("targetTemp").gameObject;
        firstPos = targetTemp.transform.position;
        middlePos = cheese.transform.position;
        lastPos = new Vector3(12f, -2.5f, 0);

        for (int i = 0; i < 3; i++)
        {
            antGo[i] = false;
            currTarget[i] = firstPos;
        }
    }

    private void Update()
    {
        if (start)
        {
            // all start after mouse clicking
            if (mouse != null && mouse.GetComponent<mouseSleepingScript>().isClicked)
                createAnt(0);

            // ants go one by one
            for (int i = 0; i < 3; i++)
            {
                if (antGo[i])
                {
                    goToTarget(i);

                    // if temporary target get, go to cheese
                    if (currTarget[i] == firstPos && targetGet(i))
                    {
                        currTarget[i] = middlePos;
                        if (i < 2)
                        {
                            createAnt(i + 1);
                            antGo[i + 1] = true;
                        }
                    }

                    // if cheese get, go to exit
                    if (currTarget[i] == middlePos && targetGet(i))
                    {
                        currTarget[i] = lastPos;
                        cheeseMove = true;
                    }
                }
            }

            // if exit get, destroy ants and cheese
            if (antGo[0] && currTarget[0] == lastPos && targetGet(0))
            {
                cheeseMove = false;
                antGo[0] = false;
                antGo[1] = false;
                antGo[2] = false;
                Destroy(ants[0]);
                Destroy(ants[1]);
                Destroy(ants[2]);
                Destroy(cheese);
            }

            // move cheese by ants
            if (cheeseMove)
            {
                Vector3 pos = ants[1].transform.position;
                pos.y += 0.7f;
                cheese.transform.position = pos;
            }
        }

        // if trap is done, ants don't go
        GameObject stick = GameObject.FindGameObjectWithTag(tags.stick);
        GameObject fence = GameObject.FindGameObjectWithTag(tags.fence);
        GameObject jumper = GameObject.FindGameObjectWithTag(tags.jumper);
        GameObject sticks = GameObject.FindGameObjectWithTag(tags.sticks);
        bool stickPut = (stick != null && stick.GetComponent<toolItem>().stop);
        bool fencePut = (fence != null && fence.GetComponent<toolItem>().stop);
        bool jumperPut = (jumper != null && jumper.transform.parent.GetComponent<toolItem>().stop);
        bool sticksPut = (sticks != null && sticks.GetComponent<toolItem>().stop);

        if (stickPut || fencePut || jumperPut || sticksPut)
            start = false;
    }

    private void createAnt(int i)
    {
        ants[i] = Instantiate(ant, transform.position, ant.transform.rotation);
        ants[i].name = "ant" + i;
        currTarget[i] = firstPos;
        antGo[i] = true;
    }

    private void goToTarget(int i)
    {
        ants[i].transform.position = Vector3.MoveTowards(ants[i].transform.position, currTarget[i], staticClass.speed * Time.deltaTime);
    }

    private bool targetGet(int i)
    {
        float antPosX = ants[i].transform.position.x;
        float antPosY = ants[i].transform.position.y;
        float targetPosX = currTarget[i].x;
        float targetPosY = currTarget[i].y;
        double distX = System.Math.Round(System.Math.Abs(antPosX - targetPosX),2);
        double distY = System.Math.Round(System.Math.Abs(antPosY - targetPosY),2);

        if (distX == 0 && distY == 0)
            return true;
        else
            return false;
    }
}
