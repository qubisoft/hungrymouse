﻿using UnityEngine;

public class brownMouseClick : MonoBehaviour
{
    private GameObject cat;
    private GameObject[] enemies;
    private GameObject mouseRuns;
    private bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        enemies = GameObject.FindGameObjectsWithTag(tags.enemy);
        foreach (GameObject enemy in enemies)
        {
            if (!enemy.GetComponent<enemyScript>().enemySO.isMouse)
                cat = enemy;
        }
        // move it slightly to allow for clicking
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -2f);
    }
    private void Update()
    {
        if (start)
        {
            if (GameObject.FindGameObjectWithTag(tags.enemyEats) != null)
            {
                gameObject.GetComponent<enemyScript>().trap();
                start = false;
            }
        }
    }

    private void OnMouseDown()
    {
        start = true;

        gameObject.GetComponent<enemyScript>().wakeUp();
        gameObject.GetComponent<BoxCollider2D>().enabled = false;

        cat.GetComponent<enemyScript>().wakeUp();

        mouseRuns = gameObject.GetComponent<enemyScript>().enemyRuns;
        mouseRuns.tag = tags.mouseRuns;
        mouseRuns.GetComponent<Rigidbody2D>().gravityScale = 0;
    }
}
