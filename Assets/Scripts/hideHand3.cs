﻿using UnityEditor;
using UnityEngine;

public class hideHand3 : MonoBehaviour
{
    // hide tip hand if stick is taken
    public GameObject hand;
    private GameObject axe;
    private GameObject handTip;
    private bool onlyOnceStick = true;
    private bool onlyOnceAxe = true;
   
    private void Update()
    {
        GameObject stick  = GameObject.FindGameObjectWithTag(tags.stick);

        if (stick != null)
        {
            if (stick.GetComponent<toolItem>().stop && onlyOnceStick)
            {
                onlyOnceStick = false;

                // hide hand
                gameObject.GetComponent<Animator>().enabled = false;
                gameObject.transform.localScale = new Vector3(0, 0, 0);

                // change toolBox
                GameObject toolBoxes = GameObject.FindGameObjectWithTag(tags.toolBoxes);
                toolBoxes.transform.GetChild(0).gameObject.SetActive(false);
                toolBoxes.transform.GetChild(1).gameObject.SetActive(true);

                // point axe
                axe = GameObject.FindGameObjectWithTag(tags.axe);
                handTip = Instantiate(hand, transform.position, Quaternion.identity);
                handTip.name = "hand";
                Vector3 pos = hand.transform.position;
                handTip.transform.position = new Vector3(pos.x + 5f, pos.y - 0.5f, pos.z);
                handTip.transform.rotation = gameObject.transform.rotation;
                //handTip.SetActive(true);
            }
        }
        if (axe != null && axe.GetComponent<toolItem>().isClicked && onlyOnceAxe)
        {
            onlyOnceAxe = false;
            Destroy(handTip);
            Destroy(gameObject);
        }
    }
}
