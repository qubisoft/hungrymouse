﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class riverScript : MonoBehaviour
{
    GameObject obj;
    GameObject cheese;
    GameObject putHere;
    GameObject jumper;
    GameObject sticks;
    GameObject stick;
    GameObject fence;
    bool bStick = false;
    bool bFence = false;
    bool bJumper = false;
    bool bSticks = false;
    bool down = false;
    bool move = false;
    bool start = false;
    Vector3 pos;
    Vector3 target;

    private void Start()
    {
        cheese = GameObject.FindGameObjectWithTag(tags.cheese);
        target = cheese.transform.position;
        target.x -= 3f;
        putHere = GameObject.FindGameObjectWithTag(tags.placeToPut);
    }
    void Update()
    {
        checkItem(ref jumper, ref bJumper, tags.jumper);
        checkItem(ref fence, ref bFence, tags.fence);
        checkItem(ref sticks, ref bSticks, tags.sticks);
        checkItem(ref stick, ref bStick, tags.stick);

        if (down)
        {
            obj.transform.position = Vector3.MoveTowards(obj.transform.position, new Vector3(pos.x, pos.y - 1f, pos.z), staticClass.speed * Time.deltaTime);
            if (obj.tag == tags.fence)
            {
                Color spriteColor = obj.GetComponent<SpriteRenderer>().color;
                spriteColor.a *= 0.95f;
                obj.GetComponent<SpriteRenderer>().color = spriteColor;
                if (System.Math.Round(obj.GetComponent<SpriteRenderer>().color.a, 1) == 0)
                {
                    Destroy(obj);
                    down = false;
                }
            }
            else
            {
                obj.transform.GetChild(0).GetComponent<Animator>().SetBool("out", true);
            }
        }

        GameObject mouse = GameObject.FindGameObjectWithTag(tags.mouseRuns);
        try
        {
            if (mouse.transform.position.x > obj.transform.position.x)
                start = true;
        }
        catch
        {
            start = false;
        }

        if (move && start)
        {
            obj.transform.position = Vector3.MoveTowards(obj.transform.position, target, staticClass.speed * Time.deltaTime);
        }
    }

    private void checkItem(ref GameObject item, ref bool check, string tag)
    {
        if (!check)
        {
            item = GameObject.FindGameObjectWithTag(tag);
            if (item != null && item.tag == tags.jumper)
                item = item.transform.parent.gameObject;

            bool checkItem = (item != null && item.GetComponent<toolItem>().stop);
            if (checkItem)
            {
                obj = item;
                pos = obj.transform.position;
                check = true;

                if (obj.tag == tags.stick || obj.tag == tags.sticks)
                {
                    move = true;
                    putHere.GetComponent<toolHere>().toolsUsed[tags.fence] = false;
                    putHere.GetComponent<toolHere>().toolsUsed[tags.jumper] = false;
                }
                else
                    move = false;

                down = !move;
            }
        }
    }
}
