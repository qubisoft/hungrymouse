﻿using UnityEngine;

public class enemyScript : MonoBehaviour
{
    public enemyScriptableObject enemySO;
    public GameObject enemyTarget;
    public string enemyStatus;  // sleeps, runs, eats, trapped

    private GameObject enemySleeps;
    public GameObject enemyRuns;

    // enemy sleeps at the beginning
    private void Start()
    { 
        enemySleeps = createObject(enemySO.enemySleeps, transform.position, "enemyRuns", tags.enemySleeps);
        enemyStatus = "sleeps";
    }

    public void wakeUp()
    {
        enemyRuns = createObject(enemySO.enemyRuns, transform.position, "enemyRuns", tags.enemyRuns);
        enemyRuns.GetComponent<enemyRuns>().enemyTarget = enemyTarget;
        enemyRuns.GetComponent<enemyRuns>().enemy = gameObject;
        enemyStatus = "runs";
        Destroy(enemySleeps);
    }

    public void eat()
    {
        GameObject enemyEats = createObject(enemySO.enemyEats, enemyRuns.transform.position, "enemyEats", tags.enemyEats);
        enemyStatus = "eats";
        Destroy(enemyRuns);
    }

    public void trap()
    {
        GameObject enemyTrapped = createObject(enemySO.enemyTrapped, enemyRuns.transform.position, "enemyTrapped", tags.enemyTrapped);
        enemyStatus = "trapped";
        Destroy(enemyRuns);
    }
    private GameObject createObject(GameObject prefab, Vector3 position, string name, string tag)
    {
        GameObject obj;
        Quaternion rot = prefab.transform.rotation;
        obj = Instantiate(prefab, position + prefab.transform.position, rot);
        obj.name = name;
        obj.tag = tag;
        obj.transform.localScale = prefab.transform.localScale;
        return obj;
    }
}
