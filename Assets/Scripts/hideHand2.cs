﻿using UnityEngine;

public class hideHand2 : MonoBehaviour
{
    // hide tip hand if stick is taken
    public GameObject hand;

    private void Update()
    {
        GameObject stick = GameObject.FindGameObjectWithTag(tags.stick);

        if (stick != null && stick.GetComponent<toolItem>().isClicked)
        {
            gameObject.SetActive(false);
            GameObject handTip = Instantiate(hand, transform.position, Quaternion.identity);
            handTip.name = "hand";
            Vector3 pos = hand.transform.position;
            handTip.transform.position = new Vector3(pos.x, pos.y + 2.5f, pos.z);
            handTip.transform.rotation = hand.transform.rotation;
        }
    }
}
