﻿using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "ScriptableObjects/enemy", order = 1)]
public class enemyScriptableObject : ScriptableObject
{
    public GameObject enemySleeps;
    public GameObject enemyRuns;
    public GameObject enemyEats;
    public GameObject enemyTrapped;
    public bool isMouse;
    public bool woolReact;
    public bool cageReact;
    public bool treeReact;
}
