﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tools", menuName = "ScriptableObjects/toolItem", order = 1)]
public class toolItemScriptableObject : ScriptableObject
{
    public Sprite img;
    public string tag;
    public GameObject anim;
    public string script;
    public float scale;
}
